# MazeGeek BD, Khulna R&D Team

This is a summary website for **MazeGeek BD, Khulna Research & Development Team**
For GitLab Repository visit [mazegeek](https://gitlab.com/tanmoy_MazeGeek/Vehicle_Detection)

## Goals

* `Vehicle Detection Dataset` - Around 10k raw images taken from phone, public domian images
* `Research Paper on Vehicle Detection` - Open Dataset, Model Architecture + Accuracy, Online Learning
* `US Car Number Plate Recognition [State wise]` - Dataset + Constructing solution for each states 
* `VIN Dataset` - Minimum 2k VIN samples
* `Automated VIN Detection System` - Research TO DO

## Timeline

    Phase 1    #    August-27, 2018 - September 14 [Car Plate Localization]
	               > August-27 Meeting
				   > Summary:
				   > The team will be working on the car plate localization
				     / recognization by experimenting on some benchmark 
					 datasets [Research Team]
				   > VIN dataset collection [Office]
				   > Remote access to training environment [Office]
	
    
    Phase 2    #    {To be decided}
        ...    #  [Yet to be decided]
		

Update 1: November X - Basic research base, previous works in the domain [Abstract, Introduction], also paper sections will be written
Update 2: November Y - Testing some new pipelines, experimenting with our novel ideas [Research methodology], also paper sections will be written
Update 3: December Z - Comparison of previous methods and our methods, improving our results, some addition in methodology section, also paper sections will be written
Update 4: December Z' - We will give a presentation on our work, you'll decide how was our performance and what things need to be improved.

`We went through many recent research papers on meta-learning and one-shot learning in the last two weeks. So, we've got a base for our research pipeline. We're trying to make our system innovative by introducing many recent techniques in machine learning - we're trying to establish an "end-to-end model which can learn from small set of examples by exploiting meta-learning mechanism and attention".
Summary of abstract: A fast system for stochastic learning of images, automatically clustering of classes. We can automatically cluster cars based on their brands, color, looks etc. So, there will be many features for a single neural net architecture, we'll also introduce additional heuristics and conditional cases.

`
